<?php
/** Enable W3 Total Cache Edge Mode */
define('W3TC_EDGE_MODE', true); // Added by W3 Total Cache


/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '02C+Vc(59)m/7~)/1B(eZ#iYZmEo0Gv.56,H0{G,7b[vV:UAP+m|<F]/KE}YmO u');
define('SECURE_AUTH_KEY',  'Jj=lQFDAA_1-ct1}jzBfWr>?:@5NsTdNSRa0vp>PdfjZ,B`.B}0fyV%6B=O4(bD%');
define('LOGGED_IN_KEY',    'cBr@fgy4Cg6N`[UV*kVXm4jp-2cN B$O|mZ9W#P,[7Tsk7o}qsN->YaQeu*}F#])');
define('NONCE_KEY',        '{?MXcASKkU?CyCfTk+k -F+r?4V{/+N|j{U?RY8cKEvg3jA}ix]-Bu}S>S+hpD#`');
define('AUTH_SALT',        '|hS>e]/aW>5v+qtGaEj ,%bRBxBTd#KWDZ`LGJLN@.]%[1;]-2{*|&Jw+j Kb<sY');
define('SECURE_AUTH_SALT', '5._)mmEAe3)z?{@^LfBc>(D[}|6i-](1W<:&$t@DDjB1P#p7fo(H&SI)fi,|xjUO');
define('LOGGED_IN_SALT',   'W%ifG%G/]LNk2tUH9y0x+PDLi!zZe7d/tA(Cg^;|-1z(~!fGc2#yDO4i7m<vpJ#%');
define('NONCE_SALT',       '+_x3I(lh>XGme|r`075]M|3BGr.+&Rw%%`|Y<kJ(JlhstmH_9*GKT.0(.-oFc/qG');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('FS_METHOD', 'direct');