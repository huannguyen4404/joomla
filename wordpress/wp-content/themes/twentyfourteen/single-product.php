<?php
/**
 * The Template for displaying all taxonomy product
 */

get_header(); 
?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();
?>
<?php if(has_post_thumbnail()):?>
 <div>
   <?php echo get_the_post_thumbnail($post->ID, 'post-thumbnail'); ?>"
 </div>
<?php endif;?>
<h2 class="entry-title">
  <a href="<?php the_permalink(); ?>"      title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?>
  </a>
</h2>
<div>
 <?php echo get_the_term_list( $post->ID, 'size', '<strong>Kích thước:</strong>', ', ', '' );?>
</div>
<div>
 <?php echo get_the_term_list( $post->ID, 'brand', '<strong>Nhãn hiệu:</strong>', ', ', '' );?>
</div>
<div>
  <?php echo get_the_term_list($post->ID, 'price', '<strong>Giá:</strong>', '', '');?>
</div>
		<?php
					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );

					// Previous/next post navigation.
					twentyfourteen_post_nav();

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}
				endwhile;
			?>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php
get_sidebar( 'content' );
get_sidebar();
get_footer();